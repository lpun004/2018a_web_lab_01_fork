A demonstration of merge conflicts
----------------------------------

In Git, "merging" is the act of integrating another branch into your current working 
branch. You're taking changes from another context (that's what a branch effectively is: 
a context) and combine them with your current working files.

A great thing about having Git as your version control system is that it makes merging 
extremely easy: in most cases, Git will figure out how to integrate new changes.

However, there's a handful of situations where you might have to step in and tell Git 
what to do. Most notably, this is when changing the same file. Even in this case, Git 
will most likely be able to figure it out on its own. But if two people changed the 
same lines in that same file, or if one person decided to delete it while the other 
person decided to modify it, Git simply cannot know what is correct. Git will then 
mark the file as having a conflict - which you'll have to solve before you can continue 
your work.

Description obtained from the Git Tower book, found [here](https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/merge-conflicts)

```
An equation:
 
a = 5
b = 7
 
a * b = 45
```

```
An equation:
 
i = 5
b = 7
 
i * b = 45
```

Solving merge conflicts
-----------------------

<<<<<<< HEAD
1. Try to let git solve the conflict automatically
2. If that failed, open the conflicting file and locate the conflict markers
3. Determine if I have to pick one version, or if I want parts from both
4. Action the change
5. Commit the changes
6. Done!
=======
1. Try to let git solve the conflict automagically
2. If that failed, try to hold back the tears
>>>>>>> ex8person2

How do we feel about merging
----------------------------

<<<<<<< HEAD
Merging is great because most of the time git can resolve merges for me. 
Sometimes I have to deal with conflicts but it isn't too bad
=======
I don't have any opinion
>>>>>>> ex8person2
